package ir.co.ke.converter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText mill,inches;
    private Button convertBtn,exitBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mill=(EditText)findViewById(R.id.mill_txt);
        inches=(EditText)findViewById(R.id.inch_txt);
        convertBtn=(Button)findViewById(R.id.convert_btn);
        exitBtn=(Button)findViewById(R.id.exit_btn);

        convertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //inches=mill/24.5;
                int millimeters=Integer.parseInt(mill.getText().toString());
                double inch=millimeters/24.5;

                inches.setText(""+inch);
            }
        });

        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
